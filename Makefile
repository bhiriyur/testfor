FC=mpif90

NPROCS=6

all: clean testmpi.exe run

testmpi.exe: testmpi.f90
	$(FC) $^ -o $@

run: testmpi.exe
	mpirun --allow-run-as-root -np $(NPROCS) ./$<
	#exit 5

clean:
	rm -rf *~ *.exe *.out
