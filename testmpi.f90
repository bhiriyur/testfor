program testmpi
  use mpi
  implicit none

  integer :: ierr, rank, size

  call MPI_INIT(ierr)
  
  call MPI_COMM_SIZE(MPI_COMM_WORLD, size, ierr)
  call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierr)

  write(*,10) size, rank
10 format("Hello World (of size: ", I3, "), I am rank: ", I3)

  call MPI_FINALIZE(ierr)

end program testmpi
